import json


def convertir_a_json_select(objeto):
    datos = []
    for obj in objeto:
        datos.append({'id': obj.id, 'nombre': obj.nombre})
    return json.dumps(datos)


def convertir_a_json_select_vda_lic(objeto):
    datos = []
    for obj in objeto:
        datos.append({'id': obj.id, 'nombre': obj.vereda.nombre})
    return json.dumps(datos)


def convertir_a_json_select_vda_lic_con_licencia(objeto):
    datos = []
    for obj in objeto:
        datos.append({'id': obj.id, 'nombre': '{0} - ({1})'.format(obj.vereda.nombre, obj.licencia.nombre)})
    return json.dumps(datos)


def convertir_select_vda_lic_con_licencia(objeto):
    datos = []
    for obj in objeto:
        datos.append({'id': obj.id, 'nombre': '{0} - ({1})'.format(obj.vereda.nombre, obj.licencia.nombre)})
    return datos