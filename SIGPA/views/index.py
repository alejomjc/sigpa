# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View


class AuthAbsView(View):
    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return super(AuthAbsView, self).dispatch(*args, **kwargs)
        else:
            return redirect(reverse('administracion:inicio-sesion'))


class IndexView(AuthAbsView):
    def get(self, request):
        return render(request, 'SIGPA/index.html')
