
'use strict';

const idiomaTabla = {
    info: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
    infoEmpty:'Mostrando registros del 0 al 0 de un total de 0 registros',
    emptyTable: 'No hay registros disponibles',
    search: 'Buscar: ',
    oPaginate: {sFirst:"Primero",sLast:"Último",sNext:"Siguiente",sPrevious:"Anterior"},
}