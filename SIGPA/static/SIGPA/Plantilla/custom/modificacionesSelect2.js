'use strict';

$(".select2").select2({
    placeholder: 'Seleccione una opción',
    "language": {
        noResults: function () {
          return 'No se encontraron coincidencias';
        },
        searching: function () {
          return 'Buscando…';
        },
        removeAllItems: function () {
          return 'Quitar todas los items';
        }
    },
});

$(window).resize(function() {
    $('.select2').css('width', "100%");
});
