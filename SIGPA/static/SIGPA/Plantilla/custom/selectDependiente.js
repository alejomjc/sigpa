
'use strict';

function cargarDatosSelect(idOrigen, idDestino, ruta, idDependientes = []) {
    $.ajax({
        url: ruta + '?selecciones=' + idOrigen.val(),
        type: 'GET',
        context: document.body,
        success: function (data) {
            if (data.length > 0) {
                let datos = JSON.parse(data);
                limpiar([idDestino].concat(idDependientes))
                for (let i = 0; i < datos.length; i++) {
                    idDestino.append('<option value="' + datos[i].id + '">' + datos[i].nombre + '</option>');
                }
            } else {
                limpiar([idDestino].concat(idDependientes))
            }
        },
        failure: function (errMsg) {
            alert('Se presentó un error al realizar la carga de datos');
        }
    });
}

function limpiar(datos){
    datos.forEach(function (d){
        d.empty();
        d.append('<option value="">Seleccione una opción</option>');
    })
}