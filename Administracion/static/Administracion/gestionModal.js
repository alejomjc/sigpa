
'use strict';

function gestionarModal (ruta, id_modal) {
    $.ajax({
        url: ruta,
        context: document.body
    }).done(function (response) {
        let modalVeredas = $('#' + id_modal);
        modalVeredas.html(response);
        modalVeredas.modal("show");
    });
}