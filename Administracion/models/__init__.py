from .models import Estado, Pais, Municipio, Departamento, CentroPoblado, Vereda, VeredaLicencia, Licencia, \
    ProcesoFundal, TipoBeneficiario, TipoAccion, LineaInversion, Beneficiario
