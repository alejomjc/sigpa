from django.db import models


class Pais(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='Nombre', blank=False, null=False)
    sigla = models.CharField(max_length=10, verbose_name='Descripción', blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Pais'
        verbose_name_plural = 'Paises'


class Departamento(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='Nombre', blank=False, null=False)
    sigla = models.CharField(max_length=10, verbose_name='Descripción', blank=False, null=False)
    pais = models.ForeignKey(Pais, on_delete=models.DO_NOTHING, verbose_name='País', blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'


class Municipio(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='Nombre', blank=False, null=False)
    sigla = models.CharField(max_length=10, verbose_name='Descripción', blank=False, null=False)
    departamento = models.ForeignKey(Departamento, on_delete=models.DO_NOTHING, verbose_name='Departamento',
                                     blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipios'


class Vereda(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='Nombre', blank=False, null=False)
    sigla = models.CharField(max_length=10, verbose_name='Descripción', blank=True, null=True)
    municipio = models.ForeignKey(Municipio, on_delete=models.DO_NOTHING, verbose_name='Municipio',
                                  blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Vereda'
        verbose_name_plural = 'Veredas'


class CentroPoblado(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='Nombre', blank=False, null=False)
    sigla = models.CharField(max_length=10, verbose_name='Descripción', blank=False, null=False)
    municipio = models.ForeignKey(Municipio, on_delete=models.DO_NOTHING, verbose_name='Municipio',
                                  blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Centro Poblado'
        verbose_name_plural = 'Centros Poblados'


class Licencia(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Licencia'
        verbose_name_plural = 'Licencias'


class VeredaLicencia(models.Model):
    vereda = models.ForeignKey(Vereda, on_delete=models.DO_NOTHING, verbose_name='Vereda',
                               blank=False, null=False)
    licencia = models.ForeignKey(Licencia, on_delete=models.DO_NOTHING, verbose_name='Licencia',
                                 blank=False, null=False)

    def __str__(self):
        return 'Vereda: {0} - Licencia {1}'.format(self.vereda, self.licencia)

    class Meta:
        verbose_name = 'Vereda Licencia'
        verbose_name_plural = 'Veredas Licencias'


class Estado(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'
    # Valores
    CREADO = 1
    EJECUCION = 2
    SUSPENDIDO = 3
    FINALIZADO = 4


class ProcesoFundal(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Proceso Fundal'
        verbose_name_plural = 'Procesos Fundales'


class TipoAccion(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tipo de Acción'
        verbose_name_plural = 'Tipos de Acciones'


class LineaInversion(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)
    proceso_fundal = models.ForeignKey(ProcesoFundal, on_delete=models.DO_NOTHING, verbose_name='Proceso Fundal',
                                       blank=False, null=False)
    tipo_accion = models.ForeignKey(TipoAccion, on_delete=models.DO_NOTHING, verbose_name='Tipo de Acción',
                                    blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Linea de Inversión'
        verbose_name_plural = 'Lineas de Inversiónes'


class TipoBeneficiario(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tipo de Beneficiario'
        verbose_name_plural = 'Tipo de Beneficiario'


class Entregable(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Entregable'
        verbose_name_plural = 'Entregables'


class Beneficiario(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    numero_personas = models.IntegerField(verbose_name='Número de Personas', blank=False, null=False)
    tipo_beneficiario = models.ForeignKey(TipoBeneficiario, on_delete=models.DO_NOTHING,
                                          verbose_name='Tipo de Beneficiario', blank=False, null=False)
    vereda_licencia = models.ForeignKey(VeredaLicencia, on_delete=models.DO_NOTHING, verbose_name='Vereda',
                                        blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Beneficiario'
        verbose_name_plural = 'Beneficiarios'

    @staticmethod
    def data_form(datos):
        beneficiario = Beneficiario()
        beneficiario.nombre = datos.POST.get('nombre', '')
        beneficiario.vereda_licencia_id = datos.POST.get('vereda', '')
        beneficiario.tipo_beneficiario_id = datos.POST.get('tipo_beneficiario', '')
        beneficiario.numero_personas = datos.POST.get('numero_personas', '')
        beneficiario.estado = datos.POST.get('estado', 'False') == 'on'
        return beneficiario


class TipoNovedad(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tipo de Novedad'
        verbose_name_plural = 'Tipo de Novedad'
