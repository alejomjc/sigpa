from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse

from Administracion.models import VeredaLicencia, Licencia, Municipio, Vereda
from SIGPA.views.index import AuthAbsView


class VeredasView(AuthAbsView):
    def get(self, request):
        veredas = VeredaLicencia.objects.all()
        return render(request, 'Administracion/Veredas/index.html',
                      {'veredas': veredas, 'menu_actual': ['ADM', 'VEREDAS']})


class VeredasCrearView(AuthAbsView):
    def get(self, request):
        licencias = Licencia.objects.all()
        municipios = Municipio.objects.all()
        return render(request, 'Administracion/Veredas/_modal_crear_editar.html',
                      {'licencias': licencias, 'municipios': municipios, 'origen': 'CREAR',
                       'menu_actual': ['ADM', 'VEREDAS']})

    def post(self, request):
        vereda = Vereda()
        vereda.nombre = request.POST.get('nombre', '')
        vereda.municipio_id = request.POST.get('municipio', '')
        vereda.estado = True
        vereda_licencia = VeredaLicencia()
        vereda_licencia.vereda = vereda
        vereda_licencia.licencia_id = request.POST.get('licencia', '')

        try:
            vereda.save()
        except Exception as e:
            messages.error(request, 'Se ha generado el error: {0}'.format(e))

        try:
            vereda_licencia.save()
        except Exception as e:
            messages.error(request, 'Se ha generado el error: {0}'.format(e))

        messages.success(request, 'Se creó la vereda correctamente.')
        return redirect(reverse('Administracion:veredas-index'))


class VeredasEditarView(AuthAbsView):
    def get(self, request, id):
        vereda_licencia = VeredaLicencia.objects.get(id=id)
        licencias = Licencia.objects.all()
        municipios = Municipio.objects.all()
        return render(request, 'Administracion/Veredas/_modal_crear_editar.html',
                      {'vereda_licencia': vereda_licencia, 'licencias': licencias,
                       'municipios': municipios, 'origen': 'EDITAR', 'menu_actual': ['ADM', 'VEREDAS']})

    def post(self, request, id):
        vereda_licencia = VeredaLicencia.objects.get(id=id)
        vereda_licencia.licencia_id = request.POST.get('licencia', '')
        vereda = Vereda.objects.get(id=vereda_licencia.vereda_id)
        vereda.nombre = request.POST.get('nombre', '')
        vereda.municipio_id = request.POST.get('municipio', '')
        vereda.estado = request.POST.get('estado', 'False') == 'on'

        try:
            vereda_licencia.save(update_fields=['licencia_id'])
        except Exception as e:
            messages.error(request, 'Se ha generado el error: {0}'.format(e))

        try:
            vereda.save(update_fields=['nombre', 'municipio_id', 'estado'])
        except Exception as e:
            messages.error(request, 'Se ha generado el error: {0}'.format(e))

        messages.success(request, 'Se guardó la vereda correctamente.')
        return redirect(reverse('Administracion:veredas-index'))

