from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse

from Administracion.models import VeredaLicencia, Municipio, Beneficiario, TipoBeneficiario
from SIGPA.views.conversiones import convertir_select_vda_lic_con_licencia
from SIGPA.views.index import AuthAbsView


class BeneficiariosView(AuthAbsView):
    def get(self, request):
        beneficiarios = Beneficiario.objects.all()
        return render(request, 'Administracion/Beneficiarios/index.html',
                      {'beneficiarios': beneficiarios, 'menu_actual': ['ADM', 'BENEFICIARIOS']})


class BeneficiariosCrearView(AuthAbsView):
    def get(self, request):
        tipos_beneficiarios = TipoBeneficiario.objects.all()
        municipios = Municipio.objects.all()
        return render(request, 'Administracion/Beneficiarios/_modal_crear_editar.html',
                      {'tipos_beneficiarios': tipos_beneficiarios, 'municipios': municipios, 'origen': 'CREAR',
                       'menu_actual': ['ADM', 'BENEFICIARIOS']})

    def post(self, request):
        beneficiario = Beneficiario.data_form(request)
        try:
            beneficiario.save()
        except Exception as e:
            messages.error(request, 'Se ha generado el error: {0}'.format(e))
            return redirect(reverse('Administracion:beneficiarios-index'))

        messages.success(request, 'Se creó el beneficiario correctamente.')
        return redirect(reverse('Administracion:beneficiarios-index'))


class BeneficiariosEditarView(AuthAbsView):
    def get(self, request, id):
        beneficiario = Beneficiario.objects.get(id=id)
        tipos_beneficiarios = TipoBeneficiario.objects.all()
        municipios = Municipio.objects.all()
        veredas = convertir_select_vda_lic_con_licencia(VeredaLicencia
                                                        .objects.filter(vereda__municipio=beneficiario.
                                                                        vereda_licencia.vereda.municipio))
        return render(request, 'Administracion/Beneficiarios/_modal_crear_editar.html',
                      {'beneficiario': beneficiario, 'tipos_beneficiarios': tipos_beneficiarios,
                       'municipios': municipios, 'veredas': veredas, 'origen': 'EDITAR',
                       'menu_actual': ['ADM', 'BENEFICIARIOS']})

    def post(self, request, id):
        beneficiario = Beneficiario.data_form(request)
        beneficiario.id = id
        try:
            beneficiario.save(update_fields=['nombre', 'vereda_licencia', 'tipo_beneficiario',
                                             'numero_personas', 'estado'])
        except Exception as e:
            messages.error(request, 'Se ha generado el error: {0}'.format(e))
            return redirect(reverse('Administracion:beneficiarios-index'))

        messages.success(request, 'Se guardó el beneficiario correctamente.')
        return redirect(reverse('Administracion:beneficiarios-index'))
