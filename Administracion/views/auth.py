from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import redirect, render, reverse
from django.views import View
from django.contrib import messages
from pyad import pyad_setdefaults, aduser, pyad
from pyad.aduser import ADUser


class InicioSesionView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect(reverse('index'))
        else:
            return render(request, 'Administracion/Autenticacion/inicio_sesion.html')

    def post(self, request):
        if request.user.is_authenticated:
            return redirect(reverse('index'))
        else:
            username: str = request.POST.get('username', '')
            password = request.POST.get('password', '')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                try:
                    messages.success(request, 'Bienvenido {0}'.format(username))
                    return redirect(reverse('index'))
                except Exception:
                    messages.error(request, 'Error al iniciar sesión')
                    logout(request)
                    return render(request, 'Administracion/Autenticacion/inicio_sesion.html')

            else:
                messages.warning(request, 'El usuario y/o la contraseña no son correctos')
                return render(request, 'Administracion/Autenticacion/inicio_sesion.html')


class CierreSesion(View):
    def get(self, request):
        autenticado = request.user.is_authenticated
        if autenticado:
            logout(request)
            messages.success(request, 'Se ha cerrado la sesión')
            return redirect(reverse('Administracion:inicio-sesion'))
        else:
            return redirect(reverse('Administracion:inicio-sesion'))


def autenticacion_de_dominio(usr, pssw):
    try:
        pyad_setdefaults(ldap_server="alcsrvdc.alcaravan.org.co", username=usr, password=pssw)
        user1 = ADUser.from_cn(usr)
        print(user1)
        return True
    except Exception as e:
        print(e)
        return False


def crear_usuario(usr):
    usuario = User.objects.create_user(username=usr, password='PssWrd.2019*', email='{0}@alcaravan.org.co'.format(usr))
    return usuario
