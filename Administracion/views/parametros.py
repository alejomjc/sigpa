import calendar
from datetime import datetime, timedelta

from django.http import JsonResponse

from Administracion.models import VeredaLicencia, Beneficiario
from SIGPA.views.conversiones import convertir_a_json_select, convertir_a_json_select_vda_lic, \
    convertir_a_json_select_vda_lic_con_licencia
from SIGPA.views.index import AuthAbsView


class ObtenerVeredasDeLicencias(AuthAbsView):
    def get(self, request):
        sel_list = obtener_datos_get_ajax(request)
        try:
            veredas = convertir_a_json_select_vda_lic(VeredaLicencia.objects.filter(licencia_id__in=sel_list))
            return JsonResponse(veredas, safe=False)
        except:
            return JsonResponse({"Error": "True"})


class ObtenerBeneficiariosDeVeredas(AuthAbsView):
    def get(self, request):
        sel_list = obtener_datos_get_ajax(request)
        try:
            beneficiarios = convertir_a_json_select(Beneficiario.objects.filter(vereda_id__in=sel_list))
            return JsonResponse(beneficiarios, safe=False)
        except:
            return JsonResponse({"Error": "True"})


class ObtenerVeredaLicenciaDeMunicipio(AuthAbsView):
    def get(self, request):
        sel_list = obtener_datos_get_ajax(request)
        try:
            veredas = convertir_a_json_select_vda_lic_con_licencia(VeredaLicencia.objects
                                                                   .filter(vereda__municipio__in=sel_list))
            return JsonResponse(veredas, safe=False)
        except:
            return JsonResponse({"Error": "True"})


def obtener_datos_get_ajax(request):
    selecciones = request.GET.get('selecciones')
    sel_list = []
    if len(selecciones) > 0:
        sel_list = selecciones.split(',')
    return sel_list


def add_months(d, meses):
    mes_nuevo = (meses + d.month) % 12
    anio_nuevo = d.year + (meses + d.month) // 12
    if mes_nuevo == 0:
        mes_nuevo = 12
        anio_nuevo -= 1
    dias_mes_nuevo = calendar.monthrange(anio_nuevo, mes_nuevo)[1]
    dias_nuevo = d.day if dias_mes_nuevo > d.day else dias_mes_nuevo
    return d.replace(year=anio_nuevo, month=mes_nuevo, day=dias_nuevo)


def add_fortnight(d, quincenas):
    return d + timedelta(days=(15*quincenas))


def add_week(d, semanas):
    return d + timedelta(days=(7*semanas))


def fijar_fecha_inicio_mes(fecha):
    return fecha.replace(fecha.year, fecha.month, 1)


def obtener_fecha_fin_de_mes(anho, mes):
    return datetime.strptime('{0}-{1}-{2}'.format(anho, mes, (calendar.monthrange(anho, mes))[1]), "%Y-%m-%d")


def mes_numero_a_letras(mes) -> str:
    if mes == 1:
        return "Ene"
    elif mes == 2:
        return "Feb"
    elif mes == 3:
        return "Mar"
    elif mes == 4:
        return "Abr"
    elif mes == 5:
        return "May"
    elif mes == 6:
        return "Jun"
    elif mes == 7:
        return "Jul"
    elif mes == 8:
        return "Ago"
    elif mes == 9:
        return "Sep"
    elif mes == 10:
        return "Oct"
    elif mes == 11:
        return "Nov"
    elif mes == 12:
        return "Dic"
