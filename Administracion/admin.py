from django.contrib import admin

from Administracion.models import LineaInversion, ProcesoFundal, TipoAccion, CentroPoblado, Vereda, Municipio, \
    Beneficiario, TipoBeneficiario, Estado, Licencia, VeredaLicencia
from Administracion.models.models import Entregable, TipoNovedad


class LineaInversionAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'descripcion', 'proceso_fundal', 'tipo_accion', 'estado')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class ProcesoFundalAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'descripcion', 'estado')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class TipoAccionAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'descripcion', 'estado')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class MunicipioAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'departamento')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class VeredaAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'municipio')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class CentroPobladoAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'municipio')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class TipoBeneficiarioAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class EstadoAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class BeneficiarioAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'numero_personas', 'tipo_beneficiario', 'vereda_licencia', 'estado')
    list_display_links = ('id', 'nombre', 'numero_personas', 'tipo_beneficiario', 'vereda_licencia', 'estado')
    search_fields = ('id', 'nombre')


class LicenciaAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class VeredaLicenciaAdmin(admin.ModelAdmin):
    list_display = ('id', 'vereda', 'licencia')
    list_display_links = ('id', 'vereda', 'licencia')
    search_fields = ('id', 'vereda', 'licencia')


class EntregableAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class TipoNovedadAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


admin.site.register(LineaInversion, LineaInversionAdmin)
admin.site.register(ProcesoFundal, ProcesoFundalAdmin)
admin.site.register(TipoAccion, TipoAccionAdmin)
admin.site.register(Municipio, MunicipioAdmin)
admin.site.register(CentroPoblado, CentroPobladoAdmin)
admin.site.register(Vereda, VeredaAdmin)
admin.site.register(Beneficiario, BeneficiarioAdmin)
admin.site.register(TipoBeneficiario, TipoBeneficiarioAdmin)
admin.site.register(Estado, EstadoAdmin)
admin.site.register(Licencia, LicenciaAdmin)
admin.site.register(VeredaLicencia, VeredaLicenciaAdmin)
admin.site.register(Entregable, EntregableAdmin)
admin.site.register(TipoNovedad, TipoNovedadAdmin)
