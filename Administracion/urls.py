from django.urls import path

from Administracion.views import auth, parametros, veredas, beneficiarios

app_name = 'Administracion'


urlpatterns = [
    path('iniciar-sesion', auth.InicioSesionView.as_view(), name='inicio-sesion'),
    path('cerrar-sesion', auth.CierreSesion.as_view(), name='cierre-sesion'),
    path('obtener-veredas-de-licencias/json', parametros.ObtenerVeredasDeLicencias.as_view(),
         name='obtener-veredas-de-licencias'),
    path('obtener-beneficiarios-de-veredas/json', parametros.ObtenerBeneficiariosDeVeredas.as_view(),
         name='obtener-beneficiarios-de-veredas'),
    path('obtener-veredas-de-municipios/json', parametros.ObtenerVeredaLicenciaDeMunicipio.as_view(),
         name='obtener-veredas-municipios'),

    path('veredas/index', veredas.VeredasView.as_view(), name='veredas-index'),
    path('veredas/crear', veredas.VeredasCrearView.as_view(), name='veredas-crear'),
    path('veredas/<int:id>/editar', veredas.VeredasEditarView.as_view(), name='veredas-editar'),
    path('beneficiarios/index', beneficiarios.BeneficiariosView.as_view(), name='beneficiarios-index'),
    path('beneficiarios/crear', beneficiarios.BeneficiariosCrearView.as_view(), name='beneficiarios-crear'),
    path('beneficiarios/<int:id>/editar', beneficiarios.BeneficiariosEditarView.as_view(), name='beneficiarios-editar'),
]
