from django.contrib.auth.models import User
from django.db import models
from Administracion.models.models import Estado, VeredaLicencia, LineaInversion, Beneficiario, ProcesoFundal, \
    Entregable, TipoNovedad


class FuenteRecursos(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Fuente de Recursos'
        verbose_name_plural = 'Fuentes de Recursos'


class CentroCostos(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Centro de Costos'
        verbose_name_plural = 'Centro de Costos'


class Proyecto (models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', blank=False, null=False)
    descripcion = models.CharField(max_length=500, verbose_name='Descripción', blank=False, null=False)
    fecha_constitucion = models.DateField(verbose_name='Fecha de Constitución', blank=False, null=False)
    fecha_inicio = models.DateField(verbose_name='Fecha de Constitución', blank=False, null=False)
    fecha_final = models.DateField(verbose_name='Fecha de Constitución', blank=False, null=False)
    criterios_exito = models.CharField(max_length=500, verbose_name='Criterios de Éxito', blank=False, null=False)
    fecha_crea = models.DateField(verbose_name='Fecha de Creación', blank=False, null=False)
    fecha_modifica = models.DateField(verbose_name='Fecha de Modificación', blank=False, null=False)
    estado = models.ForeignKey(Estado, on_delete=models.DO_NOTHING, verbose_name='Estado', blank=False, null=False)
    fuente_recursos = models.ForeignKey(FuenteRecursos, on_delete=models.DO_NOTHING, verbose_name='Fuente de Recursos',
                                        blank=False, null=False)
    n_entregas = models.IntegerField(verbose_name='Número de Entregas', blank=False, null=False)
    codigo_proyecto = models.CharField(max_length=20, verbose_name='Código de Proyecto', blank=False, null=False)
    valor = models.DecimalField(verbose_name='Valor', max_digits=12, decimal_places=2, null=False, blank=False)
    centro_costos = models.ForeignKey(CentroCostos, on_delete=models.DO_NOTHING, verbose_name='Centro de Costos',
                                      blank=False, null=False)
    longitud = models.CharField(max_length=100, verbose_name='Longitud', blank=True, null=True)
    latitud = models.CharField(max_length=100, verbose_name='Latitud', blank=True, null=True)
    proceso = models.ForeignKey(ProcesoFundal, on_delete=models.DO_NOTHING, verbose_name='Proceso',
                                blank=False, null=False)
    director = models.CharField(max_length=100, verbose_name='Director del Proyecto', blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Proyecto'
        verbose_name_plural = 'Proyectos'

    @staticmethod
    def data_form(datos):
        proyecto = Proyecto()
        proyecto.nombre = datos.get('nombre_proyecto', '')
        proyecto.director = datos.get('director_proyecto', '')
        proyecto.valor = datos.get('valor_proyecto', '')
        proyecto.fecha_constitucion = datos.get('fecha_constitucion', '')
        proyecto.fecha_inicio = datos.get('fecha_inicio', '')
        proyecto.fecha_final = datos.get('fecha_finalizacion', '')
        proyecto.descripcion = datos.get('descripcion_proyecto', '')
        proyecto.criterios_exito = datos.get('criterio_exito_proyecto', '')
        proyecto.fuente_recursos_id = datos.get('fuente_financiacion', '')
        proyecto.codigo_proyecto = datos.get('codigo_proyecto', '')
        proyecto.n_entregas = datos.get('numero_entregas_proyecto', '')
        proyecto.longitud = datos.get('longitud_proyecto', '')
        proyecto.latitud = datos.get('latitud_proyecto', '')
        proyecto.centro_costos_id = datos.get('centro_costos', '')
        proyecto.proceso_id = datos.get('proceso_proyecto', '')
        return proyecto


class Facturacion(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING, verbose_name='Proyecto',
                                 blank=False, null=False)
    valor = models.DecimalField(verbose_name='Valor', max_digits=12, decimal_places=2, null=False, blank=False)
    fecha = models.DateField(verbose_name='Fecha', blank=False, null=False)
    fecha_crea = models.DateField(verbose_name='Fecha', blank=False, null=False)
    codigo_factura = models.CharField(max_length=20, verbose_name='Código de Factura', blank=False, null=False)
    usuario_crea = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return 'Facturación: {0}'.format(self.proyecto)

    class Meta:
        verbose_name = 'Facturación'
        verbose_name_plural = 'Facturaciones'


def ruta_documentos_proy(instance, filename):
    return 'Proyectos/Documentos/{0}/{1}'.format(instance.proyecto.codigo_proyecto, filename)


class ActaEntrega(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING, verbose_name='Proyecto',
                                 blank=False, null=False)
    responsable = models.CharField(max_length=100, verbose_name='Responsable', blank=False, null=False)
    descripcion = models.CharField(max_length=500, verbose_name='Descripción', blank=False, null=False)
    fecha = models.DateField(verbose_name='Fecha', blank=False, null=False)
    fecha_crea = models.DateField(verbose_name='Fecha', blank=False, null=False)
    archivo = models.FileField(upload_to=ruta_documentos_proy, verbose_name='Documento', null=False, blank=False)
    usuario_crea = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=False, null=False)
    estado = models.BooleanField(verbose_name='Estado', blank=False, null=False)

    def __str__(self):
        return 'Acta de Entrega: {0}'.format(self.proyecto)

    class Meta:
        verbose_name = 'Acta de Entrega'
        verbose_name_plural = 'Actas de Entregas'


class ActaEntregaBeneficiario(models.Model):
    acta_entrega = models.ForeignKey(ActaEntrega, on_delete=models.DO_NOTHING, verbose_name='Acta de Entrega',
                                     blank=False, null=False)
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.DO_NOTHING, verbose_name='Beneficiario',
                                     blank=False, null=False)

    def __str__(self):
        return 'Acta de Entrega: {0} - Beneficiario: {1}'.format(self.acta_entrega, self.beneficiario)

    class Meta:
        verbose_name = 'Acta de Entrega Beneficiario'
        verbose_name_plural = 'Actas de Entregas Beneficiarios'


class EntregableActaEntrega(models.Model):
    entregable = models.ForeignKey(Entregable, on_delete=models.DO_NOTHING, verbose_name='Entrega',
                                   blank=False, null=False)
    acta_entrega = models.ForeignKey(ActaEntrega, on_delete=models.DO_NOTHING, verbose_name='Acta de Entrega',
                                     blank=False, null=False)

    def __str__(self):
        return 'Entregable: {0} - Acta de Entrega: {1}'.format(self.entregable, self.acta_entrega)

    class Meta:
        verbose_name = 'Entregable Acta de Entrega'
        verbose_name_plural = 'Entregables Actas de Entregas'


class ProyectoVeredaLicencia(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING, verbose_name='Proyecto',
                                 blank=False, null=False)
    vereda_licencia = models.ForeignKey(VeredaLicencia, on_delete=models.DO_NOTHING, verbose_name='Vereda Licencia',
                                        blank=False, null=False)

    def __str__(self):
        return 'Proyecto: {0} - Vereda Licencia: {1}'.format(self.proyecto, self.vereda_licencia)

    class Meta:
        verbose_name = 'Proyecto Vereda Licencia'
        verbose_name_plural = 'Proyectos Veredas Licencias'


class ProyectoLineaInversion(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING, verbose_name='Proyecto',
                                 blank=False, null=False)
    linea_inversion = models.ForeignKey(LineaInversion, on_delete=models.DO_NOTHING, verbose_name='LineaInversion',
                                        blank=False, null=False)

    def __str__(self):
        return 'Proyecto: {0} - Linea Inversión: {1}'.format(self.proyecto, self.linea_inversion.nombre)

    class Meta:
        verbose_name = 'Proyecto Vereda Licencia'
        verbose_name_plural = 'Proyectos Veredas Licencias'


class ProyectoBeneficiario(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING, verbose_name='Proyecto',
                                 blank=False, null=False)
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.DO_NOTHING, verbose_name='Beneficiario',
                                     blank=False, null=False)

    def __str__(self):
        return 'Proyecto: {0} - Beneficiario: {1}'.format(self.proyecto, self.beneficiario)

    class Meta:
        verbose_name = 'Proyecto Beneficiario'
        verbose_name_plural = 'Proyectos Beneficiarios'


class ProyectoNovedad(models.Model):
    tipo_novedad = models.ForeignKey(TipoNovedad, on_delete=models.DO_NOTHING, verbose_name='Tipo Novedad',
                                     blank=False, null=False)
    fecha = models.DateField(verbose_name='Fecha', blank=False, null=False)
    fecha_crea = models.DateField(verbose_name='Fecha', blank=False, null=False)
    descripcion = models.CharField(max_length=500, verbose_name='Descripción', blank=False, null=False)
    responsable = models.CharField(max_length=100, verbose_name='Responsable', blank=False, null=False)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING, verbose_name='Proyecto',
                                 blank=False, null=False)
    usuario_crea = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=False, null=False)
    porcentaje = models.CharField(max_length=5, verbose_name='Descripción', blank=False, null=False)

    def __str__(self):
        return 'Proyecto: {0} - Tipo Novedad: {1}'.format(self.proyecto, self.tipo_novedad)

    class Meta:
        verbose_name = 'Proyecto Novedad'
        verbose_name_plural = 'Proyectos Novedades'


class ProyectoNovedadBeneficiario(models.Model):
    proyecto_novedad = models.ForeignKey(ProyectoNovedad, on_delete=models.DO_NOTHING, verbose_name='Proyecto',
                                         blank=False, null=False)
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.DO_NOTHING, verbose_name='Beneficiario',
                                     blank=False, null=False)

    def __str__(self):
        return 'Proyecto Novedad: {0} - Beneficiario: {1}'.format(self.proyecto_novedad, self.beneficiario)

    class Meta:
        verbose_name = 'Proyecto Novedad Beneficiario'
        verbose_name_plural = 'Proyectos Novedades Beneficiarios'


