from django.urls import path

from Proyectos.views import views, entregas, facturacion, novedades, graficos

app_name = 'Proyectos'


urlpatterns = [
    path('index', views.ProyectosView.as_view(), name='index'),
    path('crear', views.ProyectosCrearView.as_view(), name='crear'),
    path('<int:id>/editar', views.ProyectosEditarView.as_view(), name='editar'),
    path('entregas-index', entregas.EntregasView.as_view(), name='entregas-index'),
    path('entregas-crear/<int:id>', entregas.EntregasCrearView.as_view(), name='entregas-crear'),
    path('entregas-detalle/<int:id>', entregas.EntregasModalDetalleView.as_view(), name='entregas-detalle'),
    path('facturacion-index', facturacion.FacturacionView.as_view(), name='facturacion-index'),
    path('facturacion-crear/<int:id>', facturacion.FacturacionCrearView.as_view(), name='facturacion-crear'),
    path('facturacion-detalle/<int:id>', facturacion.FacturacionModalDetalleView.as_view(), name='facturacion-detalle'),
    path('novedades-index', novedades.NovedadesView.as_view(), name='novedades-index'),
    path('novedades-crear/<int:id>', novedades.NovedadesCrearView.as_view(), name='novedades-crear'),
    path('novedades-detalle/<int:id>', novedades.NovedadesModalDetalleView.as_view(), name='novedades-detalle'),
    path('graficos-index/<int:id>', graficos.GraficosView.as_view(), name='graficos-index'),
]
