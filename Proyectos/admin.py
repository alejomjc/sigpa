from django.contrib import admin

from Proyectos.models import FuenteRecursos
from Proyectos.models.models import CentroCostos


class FuenteRecursosAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'descripcion')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


class CentroCostosAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    list_display_links = ('id', 'nombre')
    search_fields = ('id', 'nombre')


admin.site.register(FuenteRecursos, FuenteRecursosAdmin)
admin.site.register(CentroCostos, CentroCostosAdmin)
