import datetime

from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse

from Proyectos.models import Proyecto
from Proyectos.models.models import Facturacion
from SIGPA.views.index import AuthAbsView


class FacturacionView(AuthAbsView):
    def get(self, request):
        facturacion = Facturacion.objects.all()
        return render(request, 'Proyectos/Facturacion/index.html',
                      {'facturacion': facturacion,  'menu_actual': ['GP', 'FACTURACION']})


class FacturacionCrearView(AuthAbsView):
    def get(self, request, id):
        proyecto = Proyecto.objects.get(id=id)
        return render(request, 'Proyectos/Facturacion/_modal_crear_editar.html',
                      {'proyecto': proyecto, 'menu_actual': ['GP', 'FACTURACION']})

    def post(self, request, id):
        facturacion = Facturacion()
        facturacion.proyecto_id = id
        facturacion.usuario_crea = request.user
        facturacion.codigo_factura = request.POST.get('codigo_factura', '')
        facturacion.valor = request.POST.get('valor', '')
        facturacion.fecha = request.POST.get('fecha_facturacion', '')
        facturacion.fecha_crea = datetime.datetime.now()
        facturacion.estado = True
        try:
            facturacion.full_clean()
        except Exception as e:
            messages.error(request, 'Se ha generado el error: {0}'.format(e))
            return redirect(reverse('Proyectos:facturacion-index'))
        facturacion.save()
        messages.success(request, 'Se guardó la facturación correctamente.')
        return redirect(reverse('Proyectos:facturacion-index'))


class FacturacionModalDetalleView(AuthAbsView):
    def get(self, request, id):
        facturacion = Facturacion.objects.get(id=id)
        return render(request, 'Proyectos/Facturacion/_modal_detalle.html',
                      {'facturacion': facturacion})
