import datetime

from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse

from Administracion.models.models import Entregable, VeredaLicencia
from Proyectos.models import ActaEntrega, Proyecto, ProyectoBeneficiario
from Proyectos.models.models import EntregableActaEntrega, ActaEntregaBeneficiario
from SIGPA.views.index import AuthAbsView


class EntregasView(AuthAbsView):
    def get(self, request):
        entregas = ActaEntrega.objects.all()
        return render(request, 'Proyectos/Entregas/index.html',
                      {'entregas': entregas, 'menu_actual': ['GP', 'ENTREGAS']})


class EntregasCrearView(AuthAbsView):
    def get(self, request, id):
        proyecto = Proyecto.objects.get(id=id)
        entregables = Entregable.objects.all()
        beneficiarios = ProyectoBeneficiario.objects.filter(proyecto=proyecto)
        return render(request, 'Proyectos/Entregas/_modal_crear_editar.html',
                      {'proyecto': proyecto, 'beneficiarios': beneficiarios,
                       'entregables': entregables, 'menu_actual': ['GP', 'ENTREGAS']})

    def post(self, request, id):
        entrega = ActaEntrega()
        entrega.proyecto_id = id
        entrega.usuario_crea = request.user
        entrega.responsable = request.POST.get('responsable', '')
        entrega.fecha = request.POST.get('fecha_entrega', '')
        entrega.fecha_crea = datetime.datetime.now()
        entrega.estado = True
        entrega.descripcion = request.POST.get('descripcion', '')
        entrega.archivo = request.FILES.get('acta_entrega', None)

        try:
            entrega.full_clean()
        except Exception as e:
            messages.error(request, 'Se ha generado el error: {0}'.format(e))
            return redirect(reverse('Proyectos:entregas-index'))

        entrega.save()
        entregables = request.POST.getlist('entregable[]', [])
        for ent in entregables:
            EntregableActaEntrega.objects.create(entregable_id=ent, acta_entrega=entrega)

        benefiarios = request.POST.getlist('beneficiario[]', [])
        for ben in benefiarios:
            ActaEntregaBeneficiario.objects.create(beneficiario_id=ben, acta_entrega=entrega)

        messages.success(request, 'Se guardó la entrega correctamente.')
        return redirect(reverse('Proyectos:entregas-index'))


class EntregasModalDetalleView(AuthAbsView):
    def get(self, request, id):
        entrega = ActaEntrega.objects.get(id=id)
        proyectos = VeredaLicencia.objects.all()
        entregas = EntregableActaEntrega.objects.filter(acta_entrega=entrega)
        beneficiarios = ActaEntregaBeneficiario.objects.filter(acta_entrega=entrega)
        return render(request, 'Proyectos/Entregas/_modal_detalle.html',
                      {'entrega': entrega, 'proyectos': proyectos,
                       'beneficiarios': beneficiarios, 'entregas': entregas})
