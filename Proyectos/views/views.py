import datetime

from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse

from Administracion.models import LineaInversion, ProcesoFundal, Estado, Licencia, VeredaLicencia, Beneficiario
from Proyectos.models import FuenteRecursos, Proyecto
from Proyectos.models.models import CentroCostos, ProyectoLineaInversion, ProyectoBeneficiario, ProyectoVeredaLicencia
from SIGPA.views.index import AuthAbsView


class ProyectosView(AuthAbsView):
    def get(self, request):
        proyectos = Proyecto.objects.all()
        return render(request, 'Proyectos/index.html',
                      {'proyectos': proyectos, 'menu_actual': 'PROYECTOS'})


class ProyectosCrearView(AuthAbsView):
    def get(self, request):
        return render(request, 'Proyectos/crear-editar.html', datos_proyectos())

    def post(self, request):
        guardar_cambios_proyecto(request, 'CREAR')
        messages.success(request, 'Se creó el proyecto correctamente.')
        return redirect(reverse('proyectos:index'))


class ProyectosEditarView(AuthAbsView):
    def get(self, request, id):
        proyecto = Proyecto.objects.get(id=id)
        return render(request, 'Proyectos/crear-editar.html', datos_proyectos(proyecto))

    def post(self, request, id):
        guardar_cambios_proyecto(request, 'EDITAR', proy_id=id)
        messages.success(request, 'Se guardó el proyecto correctamente.')
        return redirect(reverse('proyectos:index'))


def datos_proyectos(proyecto=None):
    lineas = LineaInversion.objects.all()
    financiacion = FuenteRecursos.objects.all()
    procesos = ProcesoFundal.objects.all()
    licencias = Licencia.objects.all()
    centro_costos = CentroCostos.objects.all()

    datos = {'lineas': lineas, 'financiacion': financiacion, 'procesos': procesos,
             'licencias': licencias, 'centro_costos': centro_costos, 'origen': 'CREAR',
             'menu_actual': 'PROYECTOS'}

    if proyecto:
        val_lineas_inversion = convertir_id_a_lista(ProyectoLineaInversion
                                                    .objects.filter(proyecto=proyecto)
                                                    .values('linea_inversion_id'), 'linea_inversion_id')

        lic = ProyectoVeredaLicencia.objects.filter(proyecto=proyecto)\
            .distinct('vereda_licencia__licencia').values('vereda_licencia__licencia_id')
        val_licencias = convertir_id_a_lista(lic, atributo='vereda_licencia__licencia_id')

        veredas = VeredaLicencia.objects.filter(licencia__in=val_licencias)
        val_veredas = convertir_id_a_lista(ProyectoVeredaLicencia.objects
                                           .filter(proyecto=proyecto)
                                           .values('vereda_licencia_id'), 'vereda_licencia_id')

        val_beneficiarios = convertir_id_a_lista(ProyectoBeneficiario.objects
                                                 .filter(proyecto=proyecto)
                                                 .values('beneficiario_id'), 'beneficiario_id')
        beneficiarios = Beneficiario.objects.filter(id__in=val_beneficiarios)

        datos.update({'proyecto': proyecto, 'origen': 'EDITAR', 'veredas': veredas, 'val_veredas': val_veredas,
                      'val_licencias': val_licencias, 'val_lineas_inversion': val_lineas_inversion,
                      'beneficiarios': beneficiarios, 'val_beneficiarios': val_beneficiarios})

    return datos


def convertir_id_a_lista(objeto, atributo=''):
    lista = []
    for ob in objeto:
        if atributo:
            lista.append(ob[atributo])
        else:
            lista.append(ob['id'])
    return lista


def guardar_cambios_proyecto(request, origen, proy_id=None):
    proyecto = Proyecto.data_form(request.POST)
    if origen == 'CREAR':
        proyecto.fecha_crea = datetime.datetime.now()

    proyecto.fecha_modifica = datetime.datetime.now()

    if origen == 'CREAR':
        proyecto.estado_id = Estado.CREADO

    lineas_inversion = request.POST.getlist('linea_proyecto[]', '')
    beneficiarios = request.POST.getlist('beneficiario_proyecto[]', [])
    veredas = request.POST.getlist('vereda_proyecto[]', [])

    if len(beneficiarios) <= 0:
        return redirect(reverse('proyectos:proyectos'))

    if len(lineas_inversion) <= 0:
        return redirect(reverse('proyectos:proyectos'))

    if len(veredas) <= 0:
        return redirect(reverse('proyectos:proyectos'))

    try:
        if origen == 'CREAR':
            proyecto.full_clean()
        else:
            proyecto.full_clean(exclude=['fecha_crea', 'estado'])
    except Exception as e:
        messages.error(request, 'Se ha generado el error: {0}'.format(e))
        return redirect(reverse('proyectos:proyectos'))
    try:
        if origen == 'EDITAR':
            ProyectoVeredaLicencia.objects.filter(proyecto_id=proy_id).delete()
            ProyectoLineaInversion.objects.filter(proyecto_id=proy_id).delete()
            ProyectoBeneficiario.objects.filter(proyecto_id=proy_id).delete()
            proyecto.id = proy_id
            proyecto.save(update_fields=['nombre', 'descripcion', 'fecha_constitucion', 'fecha_inicio', 'fecha_final',
                                         'criterios_exito', 'fecha_modifica', 'fuente_recursos', 'n_entregas',
                                         'codigo_proyecto', 'valor', 'centro_costos', 'longitud', 'latitud', 'proceso',
                                         'director'])
        else:
            proyecto.save()
    except Exception as e:
        messages.error(request, 'Se ha generado el error: {0}'.format(e))
        return redirect(reverse('proyectos:proyectos'))

    try:
        for ver in veredas:
            pro_vereda = ProyectoVeredaLicencia()
            pro_vereda.proyecto = proyecto
            pro_vereda.vereda_licencia_id = ver
            pro_vereda.save()

        for li in lineas_inversion:
            pro_linea_inversion = ProyectoLineaInversion()
            pro_linea_inversion.proyecto = proyecto
            pro_linea_inversion.linea_inversion_id = li
            pro_linea_inversion.save()

        for ben in beneficiarios:
            pro_beneficiarios = ProyectoBeneficiario()
            pro_beneficiarios.proyecto = proyecto
            pro_beneficiarios.beneficiario_id = ben
            pro_beneficiarios.save()
    except Exception as e:
        messages.error(request, 'Se ha generado el error: {0}'.format(e))
        return redirect(reverse('proyectos:proyectos'))
    return
