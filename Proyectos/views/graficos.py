import json
from datetime import datetime, timedelta

from django.db.models import Sum
from django.shortcuts import render

from Administracion.views.parametros import mes_numero_a_letras, obtener_fecha_fin_de_mes, add_fortnight, add_week
from Proyectos.models import Proyecto, ActaEntrega, Facturacion
from SIGPA.views.index import AuthAbsView

NOW = datetime.now()


class GraficosView(AuthAbsView):
    def get(self, request, id):
        proyecto = Proyecto.objects.get(id=id)
        month_acta_entrega = calculo_mensual(proyecto, ActaEntrega, 'ActaEntrega')
        month_facturacion = calculo_mensual(proyecto, Facturacion, 'Facturacion')

        fortnight_acta_entrega = calculo_quincenal(proyecto, ActaEntrega, 'ActaEntrega')
        fortnight_facturacion = calculo_quincenal(proyecto, Facturacion, 'Facturacion')

        week_acta_entrega = calculo_semanal(proyecto, ActaEntrega, 'ActaEntrega')
        week_facturacion = calculo_semanal(proyecto, Facturacion, 'Facturacion')

        datos = {'mensual': {'acta_entrega': month_acta_entrega, 'facturacion': month_facturacion},
                 'quincenal': {'acta_entrega': fortnight_acta_entrega, 'facturacion': fortnight_facturacion},
                 'semanal': {'acta_entrega': week_acta_entrega, 'facturacion': week_facturacion}}

        return render(request, 'Proyectos/Graficos/index.html',
                      {'proyecto': proyecto, 'datos': datos})


def proceso_calculo(Modelo, proyecto, fecha_1, fecha_2, acumulado, origen):
    consulta = Modelo.objects.filter(proyecto=proyecto, fecha__range=[fecha_1, fecha_2])
    if origen == 'ActaEntrega':
        acumulado = acumulado + consulta.count()
        return {'acumulado': acumulado, 'valor': int((100 * acumulado) / proyecto.n_entregas)}
    elif origen == 'Facturacion':
        valor = consulta.aggregate(Sum('valor'))['valor__sum']
        acumulado = acumulado + valor if valor else 0
        return {'acumulado': acumulado, 'valor': int((100 * acumulado) / proyecto.valor)}


def calculo_mensual(proyecto, Modelo, origen):
    valores = []
    meses = []

    fecha_1 = proyecto.fecha_inicio
    fecha_2 = obtener_fecha_fin_de_mes(fecha_1.year, fecha_1.month)
    acumulado = 0
    while fecha_2.month <= int(NOW.strftime("%m")) and fecha_2.year <= int(NOW.strftime("%Y")):
        meses.append(mes_numero_a_letras(fecha_2.month))
        datos = proceso_calculo(Modelo, proyecto, fecha_1, fecha_2, acumulado, origen)
        acumulado = datos['acumulado']
        valores.append(datos['valor'])
        fecha_1 = fecha_2 + timedelta(days=1)
        fecha_2 = obtener_fecha_fin_de_mes(fecha_1.year, fecha_1.month)
    return {'meses': json.dumps(meses), 'valores': valores}


def calculo_quincenal(proyecto, Modelo, origen):
    valores = []
    quincenas = []

    fecha_1 = proyecto.fecha_inicio
    fecha_2 = add_fortnight(fecha_1, 1)
    acumulado = 0
    contador = 0
    while fecha_2.month <= int(NOW.strftime("%m")) and fecha_2.year <= int(NOW.strftime("%Y")):
        contador += 1
        quincenas.append("Quinc {0}".format(contador))
        datos = proceso_calculo(Modelo, proyecto, fecha_1, fecha_2, acumulado, origen)
        acumulado = datos['acumulado']
        valores.append(datos['valor'])
        fecha_1 = fecha_2 + timedelta(days=1)
        fecha_2 = add_fortnight(fecha_1, 1)
    return {'quincenas': json.dumps(quincenas), 'valores': valores}


def calculo_semanal(proyecto, Modelo, origen):
    valores = []
    semanas = []

    fecha_1 = proyecto.fecha_inicio
    fecha_2 = add_week(fecha_1, 1)
    acumulado = 0
    contador = 0
    while fecha_2.month <= int(NOW.strftime("%m")) and fecha_2.year <= int(NOW.strftime("%Y")):
        contador += 1
        semanas.append("Sem {0}".format(contador))
        datos = proceso_calculo(Modelo, proyecto, fecha_1, fecha_2, acumulado, origen)
        acumulado = datos['acumulado']
        valores.append(datos['valor'])
        fecha_1 = fecha_2 + timedelta(days=1)
        fecha_2 = add_week(fecha_1, 1)
    return {'semanas': json.dumps(semanas), 'valores': valores}

