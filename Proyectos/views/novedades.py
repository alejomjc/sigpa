import datetime

from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse

from Administracion.models.models import TipoNovedad
from Proyectos.models import Proyecto, ProyectoBeneficiario
from Proyectos.models.models import ProyectoNovedad, ProyectoNovedadBeneficiario
from SIGPA.views.index import AuthAbsView


class NovedadesView(AuthAbsView):
    def get(self, request):
        novedades = ProyectoNovedad.objects.all()
        return render(request, 'Proyectos/Novedades/index.html',
                      {'novedades': novedades, 'menu_actual': ['GP', 'NOVEDADES']})


class NovedadesCrearView(AuthAbsView):
    def get(self, request, id):
        proyecto = Proyecto.objects.get(id=id)
        tipos_novedades = TipoNovedad.objects.all()
        porcentajes = [{'id': 10, 'nombre': '10%'}, {'id': 20, 'nombre': '20%'}, {'id': 30, 'nombre': '30%'},
                       {'id': 50, 'nombre': '50%'}]
        beneficiarios = ProyectoBeneficiario.objects.filter(proyecto=proyecto)
        return render(request, 'Proyectos/Novedades/_modal_crear_editar.html',
                      {'proyecto': proyecto, 'beneficiarios': beneficiarios, 'porcentajes': porcentajes,
                       'tipos_novedades': tipos_novedades})

    def post(self, request, id):
        novedad = ProyectoNovedad()
        novedad.proyecto_id = id
        novedad.usuario_crea = request.user
        novedad.tipo_novedad_id = request.POST.get('tipo_novedad', '')
        novedad.responsable = request.POST.get('responsable', '')
        novedad.porcentaje = request.POST.get('porcentaje', '')
        novedad.fecha = request.POST.get('fecha_novedad', '')
        novedad.descripcion = request.POST.get('descripcion', '')
        novedad.fecha_crea = datetime.datetime.now()
        novedad.estado = True

        try:
            novedad.full_clean()
        except Exception as e:
            messages.error(request, 'Se ha generado el error: {0}'.format(e))
            return redirect(reverse('Proyectos:novedades-index'))

        novedad.save()

        benefiarios = request.POST.getlist('beneficiario[]', [])
        for ben in benefiarios:
            ProyectoNovedadBeneficiario.objects.create(beneficiario_id=ben, proyecto_novedad=novedad)

        messages.success(request, 'Se guardó la novedad correctamente.')
        return redirect(reverse('Proyectos:novedades-index'))


class NovedadesModalDetalleView(AuthAbsView):
    def get(self, request, id):
        novedad = ProyectoNovedad.objects.get(id=id)
        beneficiarios = ProyectoNovedadBeneficiario.objects.filter(proyecto_novedad=novedad)
        return render(request, 'Proyectos/Novedades/_modal_detalle.html',
                      {'novedad': novedad, 'beneficiarios': beneficiarios, })
